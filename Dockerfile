FROM alpine

ARG purge

EXPOSE 1111
ENV VERSION v0.8.0
WORKDIR /root

RUN apk update && apk upgrade
RUN apk add rust cargo curl git libsass make g++ libressl-dev

RUN git clone -b ${VERSION} --single-branch https://github.com/getzola/zola.git .
RUN git submodule update --init
RUN cargo build --release
RUN cp target/release/zola /usr/local/bin/zola

RUN if [ "$purge" != "" ] ; then apk del rust cargo curl git libsass make g++ libressl-dev && rm -rf * ; fi

CMD zola serve -i 0.0.0.0
